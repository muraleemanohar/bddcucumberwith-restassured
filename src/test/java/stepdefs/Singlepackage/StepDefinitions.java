package stepdefs.Singlepackage;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class StepDefinitions {

	private Response response;
	private ValidatableResponse json;
	private ValidatableResponse json1;
	private RequestSpecification request;

	private String GET_BOOK_BY_Identifier = "https://www.googleapis.com/books/v1/volumes";
	private static final String BASE_URL = "https://www.loginradius.com";
	String email = "abcxyzabc1@mail7.com";
	String password = "Password@1";

	private static String jsonString;

	@Given("^a book exists with an Identifier of (\\d+)$")
	public void a_book_exists_with_id(String isbn) {
		request = given().param("q", "isbn:" + isbn);

	}

	@When("^a user retrieves the book by Identifier$")
	public void a_user_retrieves_the_book_by_id() {
		try {
			response = request.when().get(GET_BOOK_BY_Identifier);

			System.out.println("response: " + response.prettyPrint());
			throw new PendingException();
		} catch (Exception e) {
			System.out.println("error message  " + e);
		}
	}

	@Then("^the status code is (\\d+)$")
	public void verify_status_code(int statusCode) {
		json = response.then().statusCode(statusCode);
	}

	@Then("^the response header contains \"([^\"]*)\" ,\"([^\"]*)\"$")
	public Boolean verify_response_header(String contentType, String value) {

		String header = response.header("Content-Type");
		if (header.contains(value))
			return true;
		else
			return false;
	}

	@Then("^response includes the following in any order$")
	public void response_includes(Map<String, String> responseFields) {

		try {
			for (Map.Entry<String, String> field : responseFields.entrySet()) {
				if (StringUtils.isNumeric(field.getValue())) {
					json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
				} else {
					json.body(field.getKey(), containsInAnyOrder(field.getValue()));
				}
			}
		} catch (Exception e) {
			System.out.println("Test  " + e);
		}
	}

	@And("^response includes the following$")
	public void response_equals(Map<String, String> responseFields) {
		try {
			for (Map.Entry<String, String> field : responseFields.entrySet()) {
				if (StringUtils.isNumeric(field.getValue())) {
					json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
				} else {
					json.body(field.getKey(), containsInAnyOrder(field.getValue()));
				}
			}
		} catch (Exception e) {
			System.out.println("Test  " + e);
		}

	}

	@And("^response includes the following details$")
	public void response_includes_the_following(Map<String, String> responseFields) {
		try {
			for (Map.Entry<String, String> field : responseFields.entrySet()) {
				if (StringUtils.isNumeric(field.getValue())) {
					json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
				} else {
					json.body(field.getKey(), containsInAnyOrder(field.getValue()));
				}
			}
		} catch (Exception e) {
			System.out.println("Test  " + e);
		}
	}

	@And("^response contains the following in any order$")
	public void response_contains_in_any_order(Map<String, String> responseFields) {
		try {
			for (Map.Entry<String, String> field : responseFields.entrySet()) {
				if (StringUtils.isNumeric(field.getValue())) {
					json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
				} else {
					json.body(field.getKey(), containsInAnyOrder(field.getValue()));
				}
			}
		} catch (Exception e) {
			System.out.println("Test  " + e);
		}
	}

}
