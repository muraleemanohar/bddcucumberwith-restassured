@Identifier1
Feature: Get book by Identifier

  @StatusCode
  Scenario: User calls web service to get a book by its Identifier
    Given a book exists with an Identifier of 9781604561142
    When a user retrieves the book by Identifier
    Then the status code is 200

  @Responsemessage
  Scenario: User calls web service to get a book by its Identifier
    Given a book exists with an Identifier of 9781604561142
    When a user retrieves the book by Identifier
    Then the status code is 200
    And response includes the following
      | totalItems |             1 |
      | kind       | books#volumes |

  @Responsemessageincludes
  Scenario: User calls web service to get a book by its Identifier
    Given a book exists with an Identifier of 9781604561142
    When a user retrieves the book by Identifier
    Then response includes the following in any order
      | items.saleInfo.country     | US                            |
      | items.volumeInfo.authors   | M. Vasnetsov                  |
      | items.volumeInfo.publisher | Nova Science Pub Incorporated |

  @Responseheader
  Scenario: User calls web service to get a book by its Identifier
    Given a book exists with an Identifier of 9781604561142
    When a user retrieves the book by Identifier
    Then the status code is 200
    And the response header contains "<Content-Type>","<application/json>"

  @Responseincludes
  Scenario: User calls web service to get a book by its Identifier
    Given a book exists with an Identifier of 9781604561142
    When a user retrieves the book by Identifier
    Then the status code is 200
    And response includes the following details
      | items.volumeInfo.printType      | BOOK          |
      | items.volumeInfo.contentVersion | preview-1.0.0 |
    And response contains the following in any order
      | items.volumeInfo.title     | Paraxial Light Beams with Angular Momentum |
      | items.volumeInfo.publisher | Nova Science Pub Incorporated              |
      | items.volumeInfo.pageCount |                                        112 |
