RESTASSURED-CUCUMBER 
	
This is demo project to implement rest api testing using the below libraries.

Prerequisites: Tools/Framework/Libraries
- Eclipse/intellij IDE
- 	JDK 8
- 	Maven build tool
- 	JUnit - test runner
- 	Cucumber - BDD/Gherkin style feature files
- 	Rest assured - Rest api verification library


BDD (Feature file / Step definition/Runner Class)
BDD requires a feature file to invoke the step definitions:
Create the scenarios in feature file as per the requirements, so each step in feature file has to match a step definition in class file;
Runner Class to execute the tests.
	
REST API
This project is aimed at calling the API to validate the details of books by ID . This is written in a feature file using Cucumber.
Each line of the scenario is tied to backend code that actually executes the line (step).


Project Details:

	1.Includes one Runner Class
	2.Includes one StepDefinition Class
	3.Includes one feature files
	
	Runner Class: 
	Name: src/test/java/runner/RunnerClass.java
	Description: To execute the tests or scenarios mentioned in the two feature files 
	
	Feature files:
	Name: src/test/resources/features/GetBookByID.feature  
		
	
	1. GetBookByID.feature:
	This feature file describes the scenarios to get the details of books by ID and validating the responses
	
	StepDefinition File:
	Name:src/test/java/stepdefs/Singlepackage/StepDefinitions.java
	Each step in the feature file is tied to the step definition file
	
	
How to Execute the project:

	Open the RunnerClass.java and click the Run icon or Right Click on the RunnerClass.java and Run as 
	JUnit Test
